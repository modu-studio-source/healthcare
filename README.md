# 셀트리온 영문

## gulpfile 셋팅
### 설치
    npm i -D gulp gulp-watch gulp-plumber gulp-connect gulp-html-ssi gulp-compass gulp-kss
    npm i -D gulp-imagemin
    npm i -D imagemin-pngquant
(gulp-imagemin, imagemin-pngquant 별도 설치)


## 폴더 구조
    node_modules/
    public/
        src/
            index.html
            shtml/
            images/
            js/
            scss/
            videos/
            fonts/
            styleguide-template/
        dist/
            index.html
            html/
            images/
            js/
            css/
            videos/
            fonts/
    styleguide/
    gulpfile.js
    package.json
    styleguide.md
