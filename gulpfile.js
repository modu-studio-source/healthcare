/*
npm i -D gulp gulp-watch gulp-plumber gulp-connect gulp-html-ssi gulp-compass gulp-kss
npm i -D gulp-imagemin
npm i -D imagemin-pngquant
*/

var gulp = require('gulp'),
    //watch = require('gulp-watch'),
    compass = require('gulp-compass'),
    plumber = require('gulp-plumber'),
    connect = require('gulp-connect'),
    includer = require('gulp-html-ssi'),
    //imagemin = require('gulp-imagemin'),
    //pngquant = require('imagemin-pngquant'),
    kss = require('gulp-kss');


var fs = require('fs');

// Path
var src = './public/src';
var dist = './public/dist';

gulp.task('serve', function() {
	connect.server({
		root: dist,
		port: 8000,
		livereload: true,
        open: {
			browser: 'chrome' // chrome(안되면 Google Chrome), iexplore, ...
		}
	});
});

gulp.task('kss', function() {
    gulp.src(src+'/scss/**/*')
    .pipe(kss({
        overview: '/styleguide.md',
        templateDirectory: src+'/styleguide-template'
    }))
    .pipe(gulp.dest('./styleguide/'))
    .pipe(connect.reload())
});
gulp.task('compass', function() {
	gulp.src(src+'/scss/**/*.{scss,sass}')
	.pipe( plumber({
		errorHandler: function (error) {
			console.log(error.message);
			this.emit('end');
		}
	}) )
	.pipe(compass({
		css: dist+'/css',
		sass: src+'/scss',
		style: 'compact' // nested, expaned, compact, compressed
	}))
	.pipe( gulp.dest(dist+'/css') )
    .pipe(connect.reload())
});

gulp.task('htmlSSI', function() {
	gulp.src(src+'/shtml/**/*.html')
		.pipe(includer())
		.pipe(gulp.dest(dist+'/html'))
        .pipe(connect.reload())
});

gulp.task('imgmin', function() {
	gulp.src([src+'/images/**/*.{png,jpg,gif}'])
		//.pipe(imagemin({
		//	progressive: true
        //    ,interlaced:true
		//	,use: [pngquant()]
		//}))
		.pipe(gulp.dest(dist+'/images/'));
});

// etc
gulp.task('font', function () {
    gulp
        .src([src+'/fonts/**/*'])
        .pipe(gulp.dest(dist+'/fonts'))
});

gulp.task('js', function () {
    gulp
        .src([src+'/js/**/*'])
        .pipe(gulp.dest(dist+'/js'))
});
gulp.task('video', function () {
    gulp
        .src([src+'/videos/**/*'])
        .pipe(gulp.dest(dist+'/videos'))
});
gulp.task('index', function () {
    gulp
        .src([src+'/*.html'])
        .pipe(gulp.dest(dist))
});


//Watch task
// gulp.task('watch',[], function() {
//     watch(src+'/shtml/**/*.html', function() {
//      gulp.start('htmlSSI');
//     });
//     watch(src+'/scss/**/*.{scss,sass}', function() {
//      gulp.start(['compass'],['kss']);
//     });
//     watch(src+'/images/**/*.{png,jpg,gif}', function() {
//       gulp.start('imgmin');
//     });
//     watch(src+'/js/**/*', function() {
//      gulp.start('js');
//     });
//     watch(src+'/*.html', function() {
//         gulp.start('index');
//     });
//     watch(src+'/videos/**/*', function() {
//      gulp.start('video');
//     });
//     watch(src+'/fonts/**/*', function() {
//      gulp.start('font');
//     });
// });

gulp.task('watch',function() {
    gulp.watch(src+'/scss/**/*.{scss,sass}',['compass','kss']);
    gulp.watch(src+'/shtml/**/*.html',['htmlSSI']);
    gulp.watch(src+'/images/**/*.{png,jpg,gif}',['imgmin']);
    gulp.watch(src+'/js/*',['js']);
    gulp.watch(src+'/*.html',['index']);
    gulp.watch(src+'/videos/**/*',['video']);
    gulp.watch(src+'/fonts/**/*',['font']);
});


gulp.task('default', ['serve','compass','htmlSSI','kss','imgmin','watch','index','js','index','video','font']);
