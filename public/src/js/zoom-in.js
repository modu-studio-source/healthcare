$(window).on("resize", function () {
    var multiView = ($(window).width() <= 668) ? 2 : 3 ;
    $('#zoomList').touchSlider({
        roll : false,
        view : multiView,
        resize : true,
        page : function(e){
            var index = 0;
            var hash = window.location.hash;
            if (hash) {
                index = /\d+/.exec(hash)[0];
                index = (parseInt(index) || 1) ;
            }
            return Math.ceil(index/multiView);
        }(),
        initComplete : function (e) {
            var _this = this;
            var paging = $(this).next("#zoomPagination");
            var item = $(this).find(" > ul > li");

            paging.html("");
            for (var i = 0; i < Math.ceil(($(item).length)/e._view); i++) {
                paging.append('<button type="button" class="btn_page">page' + (i+1) + '</button>');
            }
            paging.find(".btn_page").bind("click", function (e) {
                _this.go_page($(this).index());
            });
        },
        counter : function (e) {
            $(this).next("#zoomPagination").find(".btn_page").removeClass("active").eq(e.current-1).addClass("active");
         }
    });
}).resize();
