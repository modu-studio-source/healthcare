$(window).on("resize", function () {
    var list = $('#developList');
    var body = $('#developBody');
    var multiView = ($(window).width() <= 668) ? 3 : 6 ;
    var index = 0;
    var hash = window.location.hash;
    var item = $(list).find(" > ul > li");

    if (hash) {
        index = /\d+/.exec(hash)[0];
        index = (parseInt(index) || 1) ;
        sele($(item).eq(index-1));
        sele($(body).children().eq(index-1));
    }

    $(list).touchSlider({
        roll : false,
        view : multiView,
        resize : true,
        page : function(e){
            return Math.ceil(index/multiView);
        }(),
        initComplete : function (e) {
            var _this = this;

            $(item).find('a').each(function (i, el) {
                $(this).on('click',function(){
                    sele($(this).parent('li'));
                    sele($(body).children().eq(i));
                    window.location.hash = i+1;
                    return false;
                });
            });
        },
        btn_prev : $("#developBtnPrev"),
        btn_next : $("#developBtnNext")
    });
    function sele(el) {
        $(el).addClass('on').siblings().removeClass('on');
    }
}).resize();
